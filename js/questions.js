window.onload = function(){
    let test_id = localStorage.getItem("test_id");

    let requestOptions = {
        method: "GET",
        redirect: "follow"
    };

    let searchTitle = document.querySelector('h2');
    let i = 0;
    let j = 0;
    let answerIds = [];

    document.querySelector('.child').style.display = "none";

    /*  Get quiz title and id */

    fetch("https://printful.com/test-quiz.php?action=questions&quizId="+test_id, requestOptions)
        .then(response => response.text())
        .then(function(result) {
            let objQuestions = JSON.parse(result);

            searchTitle.textContent = objQuestions[0].title; /* initial value */
            searchTitle.setAttribute("id", objQuestions[0].id); /* initial value */

            getAnswers(); /*  Get answers initial value */

            /*  Validation + nextBtn event */
            document.getElementById('nextBtn').addEventListener(
                'click',
                function (e) {
                    if (document.querySelector('.selectedAnswer')) {
                        answerIds.push(document.querySelector('.selectedAnswer').id);
                        localStorage.setItem("answerIds", answerIds);
                        searchTitle.innerHTML = nextTitle(objQuestions);
                        searchTitle.setAttribute("id", addTitleValue(objQuestions));
                        getAnswers();
                    } else {
                        alert('Please select answer!');
                    }
                }
            );
        })
        .catch(error => console.log('error', error));

    /*  Get quiz answers and create buttons */

    function getAnswers(){
        document.querySelector('.child').style.display = "none";

        let searchTitleId = searchTitle.id;

        fetch("https://printful.com/test-quiz.php?action=answers&quizId="+test_id+"&questionId="+ searchTitleId, requestOptions)
            .then(response => response.text())
            .then(function(result) {
                let objAnswers = JSON.parse(result);
                let answers = document.getElementById('answers');
                answers.innerHTML = "";

                for (let k = 0; k < objAnswers.length; k++) {
                    let button = document.createElement("button");
                    button.innerHTML = objAnswers[k].title;
                    button.setAttribute("id", objAnswers[k].id);
                    button.setAttribute("class", "answerItem");
                    answers.appendChild(button);
                    button.onclick = function() {
                        document.querySelectorAll('.answerItem').forEach(el => {
                            el.classList.remove('selectedAnswer');
                        });
                        this.classList.add('selectedAnswer');
                    };
                    document.querySelector('.child').style.display = "block";
                }

            })
            .catch(error => console.log('error', error));
    }

    function nextTitle(obj) {
        i = i + 1;
        /*  Progress bar value & max */
        document.getElementById('progressBar').max = obj.length;
        document.getElementById('progressBar').value = i;
        if(document.getElementById('progressBar').value == document.getElementById('progressBar').max){
            window.location.replace("results.html");
        }else{
            return obj[i].title;
        }
    }

    function addTitleValue(obj){
        j = j + 1;
        if(document.getElementById('progressBar').value == document.getElementById('progressBar').max){
        }else{
            return obj[j].id;
        }
    }

};
