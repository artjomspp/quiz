window.onload = function() {
    let test_id = localStorage.getItem("test_id");
    let name = localStorage.getItem("name");
    let answerIds = localStorage.getItem("answerIds");
    let answerArray = answerIds.split(",");
    let requestOptions = {
        method: 'GET',
        redirect: 'follow'
    };
    document.querySelector('.child').style.display = "none";

    /* Make first letter capital */
    name = name.charAt(0).toUpperCase() + name.slice(1);

    document.querySelector('.main-title').innerHTML = "Thanks, " + name + '!';

    for (let i = 0; i < answerArray.length; i++) {
        answerArray[i] = "&answers[]=" + answerArray[i];
    }

    fetch("https://printful.com/test-quiz.php?action=submit&quizId=" + test_id + answerArray.join(''), requestOptions)
        .then(response => response.text())
        .then(function (result) {
            let objSubmit = JSON.parse(result);
            document.querySelector('h3').innerHTML = "You responded correctly to " + objSubmit.correct + ' out of ' + objSubmit.total + ' questions.';
            document.querySelector('.child').style.display = "block";
        })
        .catch(error => console.log('error', error));
};