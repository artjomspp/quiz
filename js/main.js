window.onload = function(){
    let dropdown =  document.querySelector("#select-list-test");
    let requestOptions = {
        method: 'GET',
        redirect: 'follow'
    };

    document.querySelector('form').style.display = "none";

    fetch("https://printful.com/test-quiz.php?action=quizzes", requestOptions)
        .then(response => response.text())
        .then(function(result) {
            let obj = JSON.parse(result);
            for (let i = 0; i < obj.length; i++) {
                let node = document.createElement("option");
                let title = document.createTextNode(obj[i].title);
                node.appendChild(title);

                document.getElementById("selectTestList").appendChild(node).setAttribute("value", obj[i].id);
                document.querySelector('form').style.display = "block";
            }
        })
        .catch(error => console.log('error', error));
};

function getData(){
    let select = document.getElementById("selectTestList");
    let test_id = select.options[select.selectedIndex].value;
    let name = document.getElementById("formNameInput").value;
    localStorage.setItem("test_id", test_id);
    localStorage.setItem("name", name);
}
