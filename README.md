# README #

### Included ###

* All source files (.html, .css, .js)

### How do I get set up? ###

* To launch a project, you need to download the project folder and launch index.html file.

### About the project ( Quiz ) ###

* The project is written in Vanilla Javascript. I also didn't use Minifier/Compressor for easy code viewing, required data from the queries stored in localStorage.
* I chose this method for this task because of the performance and simplicity.